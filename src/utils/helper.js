let sorting = (array) => {
    return array.sort((a, b) => {
        if (a > b) return 1;
        else return -1;
    });
}

let compare = (a, b) => {
    if (parseInt(a['PM2.5']) > parseInt(b['PM2.5'])) return 1;
    else return -1;
}

let average = (nums) => {
    let sum = 0;
    for (let i = 0; i < nums.length; i++) {
        sum += nums[i];
    }
    return Math.round(sum / nums.length * 100) / 100;
}

module.exports = {
    sorting,
    compare,
    average
}